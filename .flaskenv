# Environment variables needed for the configuration
# - Flask application module
FLASK_APP=lb.nightly.frontend
# - CouchDB instance(s)
COUCHDB_URL=http://localhost:5984/nightly-builds
LEGACY_COUCHDB_URL=https://lhcb-couchdb.cern.ch
# - HTTP server to retrieve nightly builds artifacts
ARTIFACTS_BASE_URL=http://localhost:8081/repository/nightly_builds_artifacts
# - HTTP server to retrieve nightly builds logs
LOGS_BASE_URL=http://localhost:8081/repository/nightly_builds_logs
# - URL for legacy artifacts
LEGACY_BASE_URL=https://lhcb-nightlies-artifacts.web.cern.ch/lhcb-nightlies-artifacts

# this is only for development
FLASK_ENV=development
SECRET_KEY=development
CERN_OAUTH_CLIENT_ID=development
CERN_OAUTH_CLIENT_SECRET=development
WEBHOOK_KEY=development
RMQPWD=dev/dev

# Tweak settings for development on current mainstream (legacy-only mode)
COUCHDB_URL=
ARTIFACTS_BASE_URL=
LOGS_BASE_URL=
