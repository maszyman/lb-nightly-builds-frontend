import os
from functools import reduce
from pathlib import Path
from string import Template

from lb.nightly.configuration import service_config

try:
    _config = service_config()
except Exception:
    _config = {}


def get_config(path, default=None):
    """
    Get config value for path (e.g. "couchdb.url")

    >>> _config = {"couchdb": {"url": "http://localhost:5984"}}
    >>> get_config("couchdb.url")
    'http://localhost:5984'
    """
    return (
        reduce(lambda d, k: d.get(k) if d else None, path.split("."), _config)
        or default
    )


COUCHDB_URL = os.environ.get("COUCHDB_URL") or get_config("couchdb.url")
ARTIFACTS_BASE_URL = (
    os.environ.get("ARTIFACTS_BASE_URL", "") or get_config("artifacts.uri", "")
).rstrip("/")
ARTIFACTS_INTERNAL_URL = os.environ.get("ARTIFACTS_INTERNAL_URL", ARTIFACTS_BASE_URL)
LOGS_BASE_URL = (
    os.environ.get("LOGS_BASE_URL", "") or get_config("logs.uri", "")
).rstrip("/")
LOGS_INTERNAL_URL = os.environ.get("LOGS_INTERNAL_URL", LOGS_BASE_URL)
GITLAB_URL = "https://gitlab.cern.ch/"
GITLAB_TOKEN = os.environ.get("GITLAB_TOKEN") or get_config("gitlab_token")
SECRET_KEY = os.environ.get("SECRET_KEY") or get_config("secret_key")
CERN_OAUTH_CLIENT_ID = os.environ.get("CERN_OAUTH_CLIENT_ID") or get_config(
    "cern_oauth.id"
)
CERN_OAUTH_CLIENT_SECRET = os.environ.get("CERN_OAUTH_CLIENT_SECRET") or get_config(
    "cern_oauth.secret"
)
WEBHOOK_KEY = os.environ.get("WEBHOOK_KEY") or get_config("webhook_key")

SLOTS_CONFIG_REPOSITORY = "https://gitlab.cern.ch/lhcb-core/LHCbNightlyConf.git"
SLOTS_LIST_URL = f"{SLOTS_CONFIG_REPOSITORY.replace('.git', '')}/-/jobs/artifacts/master/raw/slots-list.json?job=check-syntax"

CACHE_TYPE = "filesystem"
CACHE_DEFAULT_TIMEOUT = 300
CACHE_DIR = Path(__file__).parent / "cache"

LEGACY_COUCHDB_URL = (
    os.environ.get("LEGACY_COUCHDB_URL", "") or get_config("legacy.couchdb", "")
).rstrip("/")
LEGACY_BASE_URL = (
    os.environ.get("LEGACY_BASE_URL", "") or get_config("legacy.artifacts", "")
).rstrip("/")
LEGACY_INTERNAL_URL = os.environ.get("LEGACY_INTERNAL_URL", LEGACY_BASE_URL)
if "RMQPWD" not in os.environ:
    os.environ["RMQPWD"] = get_config("legacy.rabbitmq", "")

DOCS_PATH = os.environ.get("DOCS_PATH") or (
    Path(__file__).parent / "docs" / "_build" / "html"
)

SCHEDULER_GRAPH_TEMPLATE_URL = Template(
    os.environ.get("SCHEDULER_GRAPH_TEMPLATE_URL", "")
)
SCHEDULER_TASK_TEMPLATE_URL = Template(
    os.environ.get("SCHEDULER_TASK_TEMPLATE_URL", "")
)
WORKER_LOGS_TEMPLATE_URL = Template(os.environ.get("WORKER_LOGS_TEMPLATE_URL", ""))
