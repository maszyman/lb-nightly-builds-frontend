FROM python:3.8

RUN groupadd -g 1000 -r web-app && useradd -u 1000 -d /opt/web-app -g web-app web-app && \
    mkdir -p /opt/web-app && chown web-app:web-app /opt/web-app
WORKDIR /opt/web-app

COPY --chown=web-app:web-app requirements.txt utils/patch_certfile /opt/web-app/
RUN pip install -r requirements.txt && \
    ln -s `python -c "import certifi; print(certifi.where())")` cacert.pem && \
    ./patch_certfile cacert.pem

COPY --chown=web-app:web-app config.py wsgi.py gunicorn.conf.py /opt/web-app/
COPY --chown=web-app:web-app lb/ /opt/web-app/lb/
COPY --chown=web-app:web-app docs/_build/html/ /opt/web-app/docs/_build/html/

# OpenShift does not use the user id we provide
RUN chmod -R a+rwX /opt/web-app

ENV COUCHDB_URL=
ENV SSL_CERT_FILE=/opt/web-app/cacert.pem

USER web-app
EXPOSE 8080
CMD ["gunicorn", "wsgi"]
