import re
from datetime import date, datetime

import dateutil.tz
import requests
from flask import Markup, escape

from .application import app, cache

GENEVA_TZ = dateutil.tz.gettz("Europe/Zurich")


@app.template_filter("fuzzy_date")
def fuzzy_date(value):
    """Convert a date string to a fuzzy time"""
    if not value:
        return ""

    value_date = date.fromisoformat(value)
    days_ago = (datetime.now(tz=GENEVA_TZ).date() - value_date).days
    if days_ago == 0:
        fuzzy = "today"
    elif days_ago == 1:
        fuzzy = "yesterday"
    elif days_ago <= 3:
        fuzzy = f"{days_ago} days ago"
    else:
        fuzzy = None
    return Markup(f'<span title="{value}">{fuzzy}</span>') if fuzzy else value


@cache.memoize(timeout=3600)
def gitlab_mr_title(project, mr):
    """
    Retrieve from gitlab the title of a merge request.

    See https://docs.gitlab.com/ee/api/merge_requests.html#get-single-mr
    """
    return (
        requests.get(
            f"https://gitlab.cern.ch/api/v4/projects/{project.replace('/', '%2F')}/merge_requests/{mr}"
        )
        .json()
        .get("title")
    )


def gitlab_mr_link(matchobj):
    """
    Helper to format a merge request link via a ``re.sub()``.
    """
    project, mr = matchobj.group(1), int(matchobj.group(2))
    title = escape(gitlab_mr_title(project, mr))
    return (
        f'<a href="https://gitlab.cern.ch/{project}/-/merge_requests/{mr}" target="_blank" class="merge-request"'
        + (f' data-toggle="tooltip" title="{title}"' if title else "")
        + f">{project}!{mr}</a>"
    )


@app.template_filter("gitlab_links")
def gitlab_links(text, gitlab_id):
    """
    Convert special patterns in text to links to gitlab.
    """
    text = escape(text)

    # replace group/Project!123 with link to merge request
    text = re.sub(
        r"([a-zA-Z0-9-_.]+/[a-zA-Z0-9-_./]+)!(\d+)",
        gitlab_mr_link,
        text,
    )

    # we cannot link commit ids witout a gitlab_id
    if gitlab_id:
        # replace commit ids with links to gitlab
        text = re.sub(
            r"(?<!mmit: |tree: |.....@)\b([a-f0-9]{7,40})\b",
            f'<a href="https://gitlab.cern.ch/{gitlab_id}/-/commit/\\1" target="_blank">\\1</a>',
            text,
        )

    return Markup(text)


@app.template_filter("highlight_path_fragments")
def highlight_path_fragments(text):
    if ":" in text and "://" not in text:
        text = Markup(
            ":".join(
                f'<span class="path-fragment">{fragment}</span>'
                for fragment in text.split(":")
            )
        )
    return text


@app.template_filter("issue_locations")
def issue_locations(issues):
    """
    Helper to format the build issues summary for easier handling in the template.

    >>> issue_locations([
    ...         {"desc": "issue 1",
    ...          "where": {"Dir1": ["case1", "case2"],
    ...                    "Dir2": ["case3"]}},
    ...         {"desc": "issue 2",
    ...          "where": {"Dir3": ["case1"]}},
    ...         ]
    ... )
    [('issue 1', [('Dir1', 'case1', 'Dir1 #1'), ('Dir1', 'case2', 'Dir1 #2'), ('Dir2', 'case3', 'Dir2')]), ('issue 2', [('Dir3', 'case1', 'Dir3')])]
    """
    return [
        (
            issue["desc"],
            [
                (
                    section,
                    anchor,
                    f"{section} #{idx}"
                    if len(issue["where"][section]) > 1
                    else section,
                )
                for section in issue["where"]
                for idx, anchor in enumerate(issue["where"][section], 1)
            ],
        )
        for issue in issues
    ]


@app.template_filter("count_issues")
def count_issues(issues):
    """
    Helper to count the total number of issues reported.

    >>> count_issues([
    ...         {"desc": "issue 1",
    ...          "where": {"Dir1": ["case1", "case2"],
    ...                    "Dir2": ["case3"]}},
    ...         {"desc": "issue 2",
    ...          "where": {"Dir3": ["case1"]}},
    ...         ]
    ... )
    4
    """
    return sum(
        sum(len(locations) for locations in issue["where"].values()) for issue in issues
    )


def format_test_body(node):
    from itertools import chain

    yield "<table><tbody>"

    for key, value in chain(
        (item for item in node.data.items() if item[0] != "results"),
        node.data["results"].items(),
    ):
        content = value if isinstance(value, str) else str(value)
        if key == "output":
            content = f"<pre>{escape(content)}</pre>"
        elif "\n" in content and not value.startswith("<"):
            # probably a block we expect to be "<pre>"-formatted
            content = f"<pre>{escape(content)}</pre>"

        # the collapsed block shows up to 5-6 lines, so we do not collapse if we have
        # less than 8
        if content.startswith("<pre>") and content.count("\n") > 7:
            content = '<pre class="big">' + content[5:]
        yield f"<tr><th>{key}</th><td>{content}</td></tr>"

    yield "</tbody></table>"


@app.template_filter("make_treeview")
def make_treeview(tests, full=False):
    """
    Special filter to generate a tree view of a the tree of tests in TestResults.
    """
    from itertools import chain

    from .utils import TestResults

    # results = [(test.find("Name").text, test.attrib["Status"]) for test in xml.findall("./Testing/Test")]
    CLASS_FOR_STATUS = dict(
        [
            ("passed", "success"),
            ("failed", "danger"),
            ("error", "danger"),
            ("skipped", "warning"),
            ("notrun", "warning"),
            ("untested", "warning"),
        ]
    )
    SEVERITY_LEVELS = ("success", "warning", "danger")

    tests = TestResults.from_dict({"tests": tests}).tests

    def get_branch_level(node):
        level = (
            SEVERITY_LEVELS.index(CLASS_FOR_STATUS[node.data.get("Status", "passed")])
            if node.data
            else 0
        )
        for child in node.children:
            level = max(level, get_branch_level(child))
        return level

    def format_node(node):
        # FIXME: this is a very inefficient way (as it computes the level several times for the leaves)
        severity = SEVERITY_LEVELS[get_branch_level(node)]
        anchor = node.id.replace("(root).", "")
        yield f'<li class="list-group-item list-group-item-{severity} collapsed'
        if node.data:
            yield " leaf"
        yield f'" id="{anchor}"'
        if severity == "success":  # we do not show (by default) the passed tests
            yield ' style="display: none;"'
        yield (
            f'><span class="test-name"><span class="collapse-icon glyphicon"></span> {node.name} '
            f'<a href="#{anchor}" title="permanent link" class="glyphicon glyphicon-link"></a></span>'
        )
        if node.children:
            yield '<ul class="list-group">'
            for fragment in chain.from_iterable(
                format_node(child) for child in node.children
            ):
                yield fragment
            yield "</ul>"
        elif node.data:
            if "results" in node.data and "Causes" in node.data["results"]:
                yield f' <span class="causes text-info">{node.data["results"]["Causes"]}</span>'
            elif severity == "danger":
                yield f' <span class="causes text-info"><i>unknown</i></span>'

            yield (
                '<ul class="list-group data">'
                '<li class="list-group-item list-group-item-default">'
            )

            if full:
                yield from format_test_body(node)
            else:
                yield f'<span class="spinner" test_id="{anchor}">loading...</span>'

            yield "</li></ul>"

        yield "</li>"

    return Markup(
        f'<ul class="list-group tests-results" id="results">{"".join(chain.from_iterable(format_node(child) for child in tests.children))}</ul>'
    )
