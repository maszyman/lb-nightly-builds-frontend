import json
import logging
import re
from datetime import datetime
from zipfile import Path as ZipPath
from zipfile import ZipFile

import requests
from dateutil.tz import tzlocal
from flask import request, session
from werkzeug.exceptions import NotFound

from .application import app, cache, require_roles
from .models import db, get_slot_info, get_slot_summary, legacy_db
from .utils import HTTPFileObject, legacy_artifact

app.add_url_rule(
    "/api/v1/<flavour>/<slot>/<int:build_id>/", "get_slot_info", get_slot_info
)
app.add_url_rule(
    "/api/v1/<flavour>/<slot>/<int:build_id>/data", "get_slot_info", get_slot_info
)
app.add_url_rule(
    "/api/v1/<flavour>/<slot>/<int:build_id>/summary",
    "get_slot_summary",
    get_slot_summary,
)

_PROJ_SIG_RE = re.compile(
    r"checking out .* from (?:https|ssh)://(?::@|git@)?gitlab.cern.ch(?::7999|:8443)?/([a-zA-Z0-9-_.]+/[a-zA-Z0-9-_./]+)\.git"
)


def _add_missing_checkout_info(report):
    """
    Helper to add required information to checkout report from legacy builds.
    """
    if "cwd" not in report:
        # just a placeholder that should not harm
        report["cwd"] = "//--cwd-undefined--//"
        # now let's scan the messages: the first that looks like a command line
        # (old style checkout logs) tells us the current directory
        for r in report.get("records", []):
            m = re.match(r"\(([^)]+)\)\$", r["text"])
            if m:
                report["cwd"] = m.group(1)
                break
    if "gitlab_id" not in report:
        for r in report.get("records", []):
            m = _PROJ_SIG_RE.match(r["text"])
            if m:
                report["gitlab_id"] = m.group(1)
                break
    return report


@app.route("/api/v1/clear-cache")
def clear_cache():
    cache.clear()
    return {"ok": "cache cleared"}


@app.route("/api/v1/<flavour>/<slot>/<int:build_id>/<project>/checkout")
@app.route("/api/v1/<flavour>/<slot>/<int:build_id>/<project>/checkout-report.json")
def get_checkout_log(flavour, slot, build_id, project):
    try:
        try:
            doc = db()[(flavour, slot, build_id)]
        except KeyError:
            doc = None
        if doc is None:
            # legacy document
            base = app.config["LEGACY_INTERNAL_URL"]
            report_url = f"{base}/{flavour}/{slot}/{build_id}/checkout/{project}.log"
            logging.debug("request checkout report from %s", report_url)
            with requests.get(report_url, stream=True) as log:
                log.raise_for_status()
                data = {
                    "records": [
                        {
                            "level": "warning"
                            if "CONFLICT" in text or "not merged" in text
                            else "info",
                            "type": "info",
                            "text": text,
                        }
                        for text in log.iter_lines(decode_unicode=True)
                        if text.strip()
                    ]
                }

        elif app.config["LOGS_INTERNAL_URL"]:
            with requests.get(
                "{}/{}-report.json".format(
                    app.config["LOGS_INTERNAL_URL"],
                    doc["checkout"]["projects"][project]["artifact"],
                )
            ) as log:
                log.raise_for_status()
                data = log.json()

        else:
            with ZipFile(
                HTTPFileObject(
                    "{}/{}".format(
                        app.config["ARTIFACTS_INTERNAL_URL"],
                        doc["checkout"]["projects"][project]["artifact"],
                    )
                )
            ) as artifact:
                data = json.load(
                    artifact.open("/".join([project, ".logs/checkout/report.json"]))
                )

        # fill data gaps for old style checkout jobs
        data = _add_missing_checkout_info(data)

        return data, {
            "Content-Disposition": f'filename="{project}.checkout-report.json"'
        }

    except requests.HTTPError as err:
        if err.response.status_code == 404:
            raise NotFound(err.response.request.url)
        else:
            raise


@app.route("/api/v1/<flavour>/<slot>/<int:build_id>/<project>/<platform>/build")
@app.route(
    "/api/v1/<flavour>/<slot>/<int:build_id>/<project>/<platform>/build-report.json"
)
@cache.memoize(timeout=3600)
def get_build_log(flavour, slot, build_id, project, platform):
    try:
        try:
            doc = db()[(flavour, slot, build_id)]
        except KeyError:
            doc = None
        if doc is None:
            # legacy document
            base = app.config["LEGACY_INTERNAL_URL"]
            report_base_url = (
                f"{base}/{flavour}/{slot}/{build_id}/build/"
                f"{platform}/{project}/build_log/"
            )
            report_url = report_base_url + "report.json"
            logging.debug("request build report from %s", report_url)
            log = requests.get(report_url)
            log.raise_for_status()
            data = log.json()
            for section in data["sections"]:
                section["html"] = requests.get(report_base_url + section["url"]).text
            cpu = requests.get(
                f"{base}/{flavour}/{slot}/{build_id}/build/{platform}/cpuinfo.json"
            ).json()
            data["cpu"] = cpu[0] if cpu else {}
            # group repeated warnings
            issues = {"errors": {}, "warnings": {}, "coverity": {}}
            for (key, issue_type) in [
                ("errors", "error"),
                ("warnings", "warning"),
                ("coverity", "coverity"),
            ]:
                for issue in data["issues"][issue_type]:
                    issues[key].setdefault(issue["desc"], {}).setdefault(
                        issue["section_id"], []
                    ).append(issue["anchor"])
            data["issues"] = {
                key: [
                    {"desc": desc, "where": where}
                    for desc, where in issues[key].items()
                ]
                for key in issues
            }

        elif app.config["LOGS_INTERNAL_URL"]:
            with requests.get(
                "{}/{}-report.json".format(
                    app.config["LOGS_INTERNAL_URL"],
                    doc["builds"][platform][project]["artifact"],
                )
            ) as log:
                log.raise_for_status()
                data = log.json()

        else:
            with ZipFile(
                HTTPFileObject(
                    "{}/{}".format(
                        app.config["ARTIFACTS_INTERNAL_URL"],
                        doc["builds"][platform][project]["artifact"],
                    )
                )
            ) as artifact:
                data = json.load(
                    artifact.open(
                        "/".join([project, ".logs", platform, "build/report.json"])
                    )
                )

        return data, {
            "Content-Disposition": f'filename="{project}.{platform}.build-report.json"'
        }

    except requests.HTTPError as err:
        if err.response.status_code == 404:
            raise NotFound(err.response.request.url)
        else:
            raise


@app.route("/api/v1/<flavour>/<slot>/<int:build_id>/<project>/<platform>/tests")
@app.route("/api/v1/<flavour>/<slot>/<int:build_id>/<project>/<platform>/Test.xml")
def get_tests_log(flavour, slot, build_id, project, platform):
    try:
        try:
            doc = db()[(flavour, slot, build_id)]
        except KeyError:
            doc = None
        if doc is None:
            # legacy document
            base = app.config["LEGACY_INTERNAL_URL"]
            report_url = f"{base}/" + legacy_artifact(
                "tests", flavour, slot, build_id, project, None, platform
            )
            logging.debug("request build report from %s", report_url)

            try:
                f = HTTPFileObject(report_url)
                f.size  # implicitly check if the remote file exists
                with ZipFile(f) as artifact:
                    name = next(
                        name
                        for name in artifact.namelist()
                        if name.endswith("Test.xml")
                    )
                    data = artifact.open(name).read()

                del f
            except requests.HTTPError as err:
                if err.response.status_code == 404:
                    raise NotFound(report_url)
                else:
                    raise
            except StopIteration:  # the zip does not contain any "Test.xml"
                raise NotFound("Test.xml")

        elif app.config["LOGS_INTERNAL_URL"]:
            with requests.get(
                "{}/{}-Test.xml".format(
                    app.config["LOGS_INTERNAL_URL"],
                    doc["builds"][platform][project]["artifact"],
                )
            ) as log:
                log.raise_for_status()
                data = log.content

        else:
            with ZipFile(
                HTTPFileObject(
                    "{}/{}".format(
                        app.config["ARTIFACTS_INTERNAL_URL"],
                        doc["tests"][platform][project]["artifact"],
                    )
                )
            ) as artifact:
                data = artifact.open(
                    "/".join([project, ".logs", platform, "tests/Test.xml"])
                ).read()

        return data, {
            "Content-Type": "text/xml",
            "Content-Disposition": f'filename="{project}.{platform}.Test.xml"',
        }

    except Exception:
        raise


def get_tests_summary_ctest(flavour, slot, build_id, project, platform):
    from .utils import summarize_test_report

    return summarize_test_report(
        get_tests_log(flavour, slot, build_id, project, platform)[0]
    )


def get_tests_summary_cmt(flavour, slot, build_id, project, platform):
    from .utils import summarize_test_report_cmt

    base = app.config["LEGACY_INTERNAL_URL"]
    report_url = f"{base}/" + legacy_artifact(
        "tests-html", flavour, slot, build_id, project, None, platform
    )
    logging.debug("request build report from %s", report_url)

    try:
        f = HTTPFileObject(report_url)
        f.size  # implicitly check if the remote file exists
        return summarize_test_report_cmt(ZipPath(f, project))
    except requests.HTTPError as err:
        if err.response.status_code == 404:
            raise NotFound(report_url)
        else:
            raise


@app.route("/api/v1/<flavour>/<slot>/<int:build_id>/<project>/<platform>/tests-report")
@app.route(
    "/api/v1/<flavour>/<slot>/<int:build_id>/<project>/<platform>/tests-report.json"
)
@cache.memoize(timeout=3600)
def get_tests_summary(flavour, slot, build_id, project, platform):
    try:
        try:
            doc = db()[(flavour, slot, build_id)]
        except KeyError:
            doc = legacy_db(flavour)[f"{slot}.{build_id}"]

        if doc.get("config", {}).get("build_tool") == "cmt":
            report = get_tests_summary_cmt(flavour, slot, build_id, project, platform)
        else:
            report = get_tests_summary_ctest(flavour, slot, build_id, project, platform)
        return (
            report,
            {
                "Content-Disposition": f'filename="{project}.{platform}.tests-report.json"'
            },
        )
    except Exception:
        raise


@app.route("/api/v1/slots/list")
def get_slots_list():
    if not app.config.get("SLOTS_LIST_URL"):
        return {}
    return requests.get(app.config["SLOTS_LIST_URL"]).json()


@app.route("/api/v1/slots/start", methods=["POST"])
def start_slots():
    flavour = request.form.get("flavour", "nightly")
    commit = request.form.get("commit", "master")
    slots = request.form.getlist("slots")
    logging.warning(
        "wanting to trigger %s slots %s from configuration %s", flavour, slots, commit
    )
    return "not implemented", 500


@app.route("/api/v1/<flavour>/<slot>/<int:build_id>/abort", methods=["POST"])
@require_roles("builder")
def abort_slot(flavour, slot, build_id):
    def flag_abort(doc):
        if not doc.get("aborted"):
            doc["aborted"] = {
                "user": session.get("username", "unkown"),
                "time": str(datetime.now(tzlocal())),
            }

    if (flavour, slot, build_id) in db():
        doc = db()[(flavour, slot, build_id)]
    elif f"{slot}.{build_id}" in legacy_db(flavour):
        doc = legacy_db(flavour)[f"{slot}.{build_id}"]
    else:
        raise NotFound(f"{flavour}/{slot}/{build_id}")

    while True:
        flag_abort(doc)
        try:
            doc.save()
            break
        except requests.HTTPError as err:
            if "Conflict" not in str(err):
                raise
            doc.fetch()

    info = dict(doc["aborted"])
    info["ok"] = "slot aborted"
    return info
