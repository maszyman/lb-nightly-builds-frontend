import functools
import re

import requests
from authlib.integrations.flask_client import OAuth
from flask import Flask, Response, redirect, request, session, url_for
from flask_caching import Cache

app = Flask(__name__, instance_relative_config=True)
app.config.from_object("config")

cache = Cache(app)

oauth = OAuth(app, cache=cache)

oauth.register(
    name="cern_oauth",
    # client_id and client_secret from configuration
    server_metadata_url="https://auth.cern.ch/auth/realms/cern/.well-known/openid-configuration",
    client_kwargs={"scope": "openid profile email"},
)


@app.route("/login")
def login():
    referer = request.headers.get("Referer")
    # prevent infinite redirection loop
    if referer and not re.match(r".*/(login|authorize|logout)", referer):
        session["login_referer"] = referer

    redirect_uri = url_for("authorize", _external=True)
    if app.config["ENV"].lower() == "development":
        return redirect(redirect_uri)

    return oauth.cern_oauth.authorize_redirect(redirect_uri)


@app.route("/authorize")
def authorize():
    # handle the case the login was not authorized
    if "code" in request.args:
        token = oauth.cern_oauth.authorize_access_token()
        userinfo = oauth.cern_oauth.parse_id_token(token)
        session["username"] = userinfo.get("cern_upn", "unknown")
        session["roles"] = userinfo.get("cern_roles", [])
        session["id_token"] = token["id_token"]
    elif app.config["ENV"].lower() == "development":
        session["username"] = "<developer>"
        session["roles"] = ["developer", "builder"]

    return redirect(session.pop("login_referer", None) or url_for("index"))


@app.route("/logout")
def logout():
    # clean up user data from session
    for key in ["username", "roles"]:
        if key in session:
            del session[key]
    try:
        id_token = session.pop("id_token")
        # this is probably needed only when the application is reloaded after login,
        # but it does not harm
        if "end_session_endpoint" not in oauth.cern_oauth.server_metadata:
            oauth.cern_oauth.load_server_metadata()
        requests.get(
            oauth.cern_oauth.server_metadata["end_session_endpoint"],
            params={"id_token_hint": id_token},
        )
    except KeyError:
        pass  # token not in session, no need to invalidate it
    return redirect(request.headers.get("Referer") or url_for("index"))


def require_roles(*roles):
    """
    Decorator to limit access to specific roles.

    Usage::

        @app.route("/secret")
        @require_roles("builder", "manager")
        def secret():
            return "top secret info"

    """
    roles = set(roles)
    assert roles, "at least one role must be specified"

    def decorator(view):
        @functools.wraps(view)
        def wrapped_view(**kwargs):
            if not roles.intersection(session.get("roles", [])):
                login_url = url_for("login")
                return Response(
                    'Unauthorized: <a href="{}">login</a> or go to <a href="{}">main page</a>'.format(
                        login_url,
                        url_for("main", flavour=kwargs.get("flavour", "nightly")),
                    ),
                    status=401,
                    headers={"WWW-Authenticate": login_url},
                )
            return view(**kwargs)

        return wrapped_view

    return decorator


@app.route("/robots.txt")
def robots():
    """
    Prevent search engins from crawling the site.

    See https://moz.com/learn/seo/robotstxt
    """
    return "User-agent: *\nDisallow: /\n", {"Content-Type": "text/plain"}
