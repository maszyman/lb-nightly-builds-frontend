from . import (
    api,
    application,
    badges,
    compare,
    filters,
    legacy_endpoints,
    logs,
    periodic_tests,
    views,
    webhooks,
)
from .application import app
