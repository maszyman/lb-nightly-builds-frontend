import math
import re
from itertools import chain

from anybadge import Badge
from flask import redirect, request, url_for
from werkzeug.exceptions import NotFound

from .application import app
from .models import get_slot_info


@app.route("/badges/<flavour>/<slot>.<int:build_id>/<kind>.svg")
@app.route("/badges/<slot>.<int:build_id>/<kind>.svg")
def backward_compatible_badge(slot, build_id, kind, flavour="nightly"):
    return redirect(
        url_for(
            f"badge",
            flavour=flavour,
            slot=slot,
            build_id=build_id,
            kind=kind,
            platform=request.args.get("platform"),
        )
    )


ALLOWED_BADGES = set("".join([a, b]) for a in ("build", "tests") for b in ("", "-diff"))


@app.route("/<flavour>/<slot>/<int:build_id>/<kind>.svg")
def badge(flavour, slot, build_id, kind):
    if kind not in ALLOWED_BADGES:
        raise NotFound(f"unknown badge type {kind}")
    if kind.endswith("-diff"):
        return diff_badge(flavour, slot, build_id, kind[:-5])
    else:
        return slot_badge(flavour, slot, build_id, kind)


COLORS = {
    "pass": ("#476c47", "#398439"),
    "warn": ("#a28152", "#f0ad4e"),
    "fail": ("#803f3d", "#ac2925"),
    "n/a": ("#dddddd", "#dddddd"),
}
"""dict: Named colors for badges.

The values are color pairs for still-in-progress and finished status.
"""


class BadSlotInfo(Exception):
    """Slot info dictionary is malformed."""

    pass


def extract_status(slot_info):
    """Extract build/tests status and problems from slot info.

    The build and test results are collected into multiple dicts where
    the key is the smallest atomic thing that can be completed or not,
    or pass or not.

    The returned status gives the completion for every project/platform
    combination. The key for the status of build and tests is
    (platform, project) and the value is True or False.

    The returned problems give the number of failures for each atomic
    thing that is tracked in the slot info. The key for retcodes, errors
    and warnings is (platform, project) while for tests the key also
    includes the test name.

    The output format is best shown with the following example:
    ```
    status = {
        'build': {
            (u'x86_64-centos7-gcc8-dbg', u'Alignment'): True,
            # ...
            (u'x86_64-centos7-gcc8-opt', u'Vetra'): True
        },
        'tests': {
            (u'x86_64-centos7-gcc8-dbg', u'Alignment'): True,
            # ...
            (u'x86_64-centos7-gcc8-opt', u'Vetra'): True
        }
    }
    problems = {
        'retcode': {
            (u'x86_64-centos7-gcc8-dbg', u'Alignment'): 0,
            # ...
            (u'x86_64-centos7-gcc8-opt', u'Vetra'): 1
        },
        'errors': {
            (u'x86_64-centos7-gcc8-dbg', u'Alignment'): 0,
            # ...
            (u'x86_64-centos7-gcc8-opt', u'Vetra'): 3
        },
        'warnings': {
            (u'x86_64-centos7-gcc8-dbg', u'Alignment'): 0,
            # ...
            (u'x86_64-centos7-gcc8-opt', u'Vetra'): 10
        },
        'tests': {
            (u'x86_64-centos7-gcc8-opt', u'Alignment', u'AlignmentDBVisualisationTool.importmatplotlib'): 0,
            (u'x86_64-centos7-gcc8-opt', u'Alignment', u'AlignmentSys.configurables'): 1,
            # ...
        }
    }
    ```

    Args:
        slot_info (dict): Slot info dictionary.

    Returns:
        (dict, dict): Two dictionaries of status and problems.

    """
    try:
        projects = slot_info["config"]["projects"]
        platforms = slot_info["config"]["platforms"]
    except KeyError:
        raise BadSlotInfo()

    status = {"build": {}, "tests": {}}
    problems = {"retcode": {}, "errors": {}, "warnings": {}, "tests": {}}

    for platform in platforms:
        for project in projects:
            if project["disabled"]:
                continue
            key = (platform, project["name"])  # common key

            # collect build results
            build_info = (
                slot_info.get("builds", {}).get(platform, {}).get(project["name"], {})
            )
            status["build"][key] = bool(build_info.get("completed"))
            if status["build"][key]:
                problems["warnings"][key] = build_info["warnings"]
                problems["errors"][key] = build_info["errors"]
                problems["retcode"][key] = int(build_info["retcode"] != 0)

            if project.get("no_test", False):
                continue

            # collect test results
            tests_info = (
                slot_info.get("tests", {}).get(platform, {}).get(project["name"], {})
            )
            status["tests"][key] = bool(tests_info.get("completed"))
            if status["tests"][key]:
                for result, test_names in list(tests_info["results"].items()):
                    for test_name in test_names:
                        problems["tests"][key + (test_name,)] = (
                            1 if result in ("FAIL", "ERROR") else 0
                        )

    return status, problems


def filter_platforms(results, platform_re):
    """Select only results (status/problems) for platforms passing a regex."""
    return {
        kind: {
            key: result
            for key, result in list(kind_results.items())
            if re.match(platform_re, key[0])
        }
        for kind, kind_results in list(results.items())
    }


def progress(x):
    """Return progress based on number of truthy iterable elements."""
    x = list(map(bool, x))
    fraction = sum(x) / len(x) if len(x) > 0 else 0.0
    if fraction > 0.0 and fraction < 0.01:
        # if we just started, return a non-zero value
        return 0.01
    return int(math.floor(fraction * 100))


def badge(prefix, progress, label, color_name):
    """Create a Badge using a progress and a color name."""
    color = COLORS[color_name][progress >= 100]
    label = label + ("  " if len(label) <= 6 else " " if len(label) <= 12 else "")
    return Badge(prefix + " {}%".format(progress), label, default_color=color)


def build_slot_badge(status, problems):
    """Return a badge with progress and number of project build failures."""
    errors = len(
        set(k for k, v in list(problems["errors"].items()) if v > 0)
        | set(k for k, v in list(problems["retcode"].items()) if v > 0)
    )
    warnings = sum(v > 0 for v in list(problems["warnings"].values()))
    if errors:
        label, color = "failing: {} errors".format(errors), "fail"
    elif warnings:
        label, color = "passing: {} warnings".format(warnings), "warn"
    elif not sum(bool(x) for x in status["build"].values()):
        # no build running
        label, color = "n/a", "n/a"
    else:
        label, color = "passing", "pass"
    return badge("build", progress(list(status["build"].values())), label, color)


def tests_slot_badge(status, problems):
    """Return a badge with progress and number of test failures."""
    failing = sum(problems["tests"].values())
    passing = len(problems["tests"]) - failing
    label = "✔ {} | ✘ {}".format(passing, failing)  # ✔ 20 | ✘ 1 | ➟ 1
    if not sum(bool(x) for x in status["tests"].values()):
        # no test run
        color = "n/a"
    else:
        color = "fail" if failing else "pass"
    return badge("tests", progress(list(status["tests"].values())), label, color)


def combined_status(status1, status2):
    """Return the combined completion status for two slots."""
    return {
        job: {
            k: status1[job].get(k, True) and status2[job].get(k, True)
            for k in chain(list(status1[job].keys()), list(status2[job].keys()))
        }
        for job in ["build", "tests"]
    }


def diff_problems(old, new, status_diff):
    """Return the difference of problems for completed parts.

    The results is a dict of tuples, where each contains the number of
    new problems, fixed problems and the number of removed items.
    For example,
    - If two new errors appear for a project in two platforms, the
      errors diff will be (4, 0, 0), and if there were no errors
      previously, the retcodes diff will be (2, 0, 0).
    - If two warnings are added in one project and one is fixed in
      another, the diff will be (2, 1, 0).
    - If ten tests are removed for two platforms, the tests diff
      will be (0, 0, 10) irrespective of the initial number of failures.

    ```
    {
        'retcode': (2, 0, 0),
        'errors': (4, 0, 0),
        'warnings': (2, 1, 0),
        'tests': (0, 0, 10)
    }
    ```

    """
    diff = {}
    for kind in new:
        status = status_diff["tests"] if kind == "tests" else status_diff["build"]
        diff[kind] = (
            # new problems (actually, more problems)
            sum(
                max(n - old[kind].get(key, 0), 0)
                for key, n in list(new[kind].items())
                if status[key[:2]]
            ),
            # fixed problems (maybe some remain, maybe some are removed)
            sum(
                max(old[kind].get(key, 0) - n, 0)
                for key, n in list(new[kind].items())
                if status[key[:2]]
            ),
            # removed items (removed project, removed failing test)
            sum(key not in new[kind] for key in old[kind] if status[key[:2]]),
        )
    return diff


def format_diff(triplet, symbols=["+", "−", "⨯"]):
    """Return a formatted diff summary of the form "+0 −1 ⨯2"."""
    return " ".join(s + str(n) for s, n in zip(symbols, triplet) if n)


def build_diff_badge(status, diff):
    """Return a badge with progress and build problems diff."""
    parts = []
    if format_diff(diff["errors"]):
        parts.append(format_diff(diff["errors"]))
    if format_diff(diff["warnings"]):
        parts.append(format_diff(diff["warnings"]))
    label = " | ".join(parts) if parts else "OK"
    if diff["errors"][0] or diff["retcode"][0]:
        color = "fail"
    elif diff["warnings"][0]:
        color = "warn"
    else:
        color = "pass"
    return badge("𝚫 build", progress(list(status["build"].values())), label, color)


def tests_diff_badge(status, diff):
    """Return a badge with progress and tests problems diff."""
    if any(diff["tests"]):
        label = format_diff(diff["tests"], ["−", "+", "⨯"])
        color = "fail" if diff["tests"][0] else "warn"
    else:
        label, color = "OK", "pass"
    return badge("𝚫 tests", progress(list(status["tests"].values())), label, color)


def slot_badge(flavour, slot, build_id, kind):
    """Slot badge view function.

    Propagates ETag to DB request for slot info and does not recreate
    badge if the info did not change.

    """
    doc = get_slot_info(flavour, slot, build_id)

    try:
        status, problems = extract_status(doc)
    except BadSlotInfo:
        raise NotFound("bad slot info")

    platform_re = request.args.get("platform")
    if platform_re:
        problems = filter_platforms(problems, platform_re)
        status = filter_platforms(status, platform_re)
        if not status["build"]:
            raise NotFound("no matching platforms")

    badge_maker = {"build": build_slot_badge, "tests": tests_slot_badge}[kind]
    return str(badge_maker(status, problems)), {"Content-Type": "image/svg+xml"}


def diff_badge(flavour, slot, build_id, kind):
    """Slot diff badge view function.

    Propagates ETag to DB request for slot info and does not recreate
    badge if the info did not change.

    """

    ref = request.args.get("ref")
    if not ref:
        raise NotFound("must provide ref parameter")

    # split ref in components
    ref_flavour = flavour  # by default we comare same flavours
    if "/" in ref:  # assume [flavour/]slot/build_id
        ref_slot, ref_build_id = ref.rsplit("/", 1)
        ref_build_id = int(ref_build_id)
        if "/" in ref_slot:
            ref_flavour, ref_slot = ref.rsplit("/", 1)
    else:  # assume legacy slot.build_id
        ref_slot, ref_build_id = ref.rsplit(".", 1)

    doc_ref = get_slot_info(ref_flavour, ref_slot, ref_build_id)
    doc = get_slot_info(flavour, slot, build_id)

    try:
        status_ref, problems_ref = extract_status(doc_ref)
        status, problems = extract_status(doc)
    except BadSlotInfo:
        raise NotFound("bad slot info")

    diff_s = combined_status(status_ref, status)
    diff = diff_problems(problems_ref, problems, diff_s)
    badge_maker = {"build": build_diff_badge, "tests": tests_diff_badge}[kind]
    return str(badge_maker(diff_s, diff)), {"Content-Type": "image/svg+xml"}
