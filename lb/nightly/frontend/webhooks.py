from flask import request
from requests import HTTPError
from werkzeug.exceptions import InternalServerError, Unauthorized

from .application import app
from .models import legacy_db


@app.route("/jenkins/", methods=["POST"])
def jenkins_hook():
    """
    If the request carries the right token, and the data looks like a MR message with
    "/ci-test" in it, we add the data to a special document in CouchDB (legacy), so
    that a Jenkins task can pick it up and trigger the actual builds.
    """
    # check if the client has the right token
    if request.headers.get("X-Gitlab-Token") != app.config.get("WEBHOOK_KEY"):
        raise Unauthorized()

    # check if the payload is a MR message with "/ci-test" in the text
    if request.json and "/ci-test" in request.json.get("object_attributes", {}).get(
        "note", ""
    ):
        # get the CouchDB document to update (create if not there)
        db = legacy_db("nightly")
        doc = (
            db["ci-tests"]
            if "ci-tests" in db
            else db.create_document(
                {"_id": "ci-tests", "type": "ci-tests", "requests": []}
            )
        )

        # update and publish (up to 5 attempts)
        for _ in range(5):
            try:
                doc["requests"].append(request.json)
                doc.save()
                break
            except HTTPError as err:
                if "conflict" not in str(err).lower():
                    raise
                doc.fetch()
        else:
            raise InternalServerError("failed to update the database")

        return "request queued"
    else:
        return "nothing to do"
