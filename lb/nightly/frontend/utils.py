import base64
import json
import logging
import os
import pathlib
import zipfile
import zlib
from itertools import groupby
from typing import Union

import requests
from flask import Markup


class HTTPFileObject:
    """
    Present the data at a given URL as a seekable file object that can be used
    as input to classes like ZipFile.

    The data is read from the URL in chunks only for the requested chunks, and
    the read chunks are cached in memory in case they have to be accessed again.
    """

    # how many bytes to read at a time
    CHUNK_SIZE = 50 * 1024 * 1024  # 50 MB

    def __init__(self, url):
        self.url = url
        self.closed = False
        self._size = None
        self._pos = 0
        self._chunks = {}
        self._logger = logging.getLogger("HTTPFileObject")

    @property
    def size(self):
        if self._size is None:
            self._logger.debug("getting size of %s", self.url)
            r = requests.head(self.url, allow_redirects=True)
            r.raise_for_status()
            self._size = int(r.headers["Content-Length"])
        return self._size

    def __len__(self):
        return self.size

    def seekable(self):
        return True

    def writable(self):
        return False

    def __del__(self):
        self.close()

    def close(self):
        self._logger.debug("summary for %s", self.url)
        self._logger.debug(" chunks: %d", len(self._chunks))
        self._logger.debug(
            " size in memory: %d", sum(len(c) for c in self._chunks.values())
        )

    def tell(self):
        return self._pos

    def seek(self, offset, whence=os.SEEK_SET):
        if whence == os.SEEK_SET:
            self._pos = offset
        elif whence == os.SEEK_CUR:
            self._pos += offset
        elif whence == os.SEEK_END:
            self._pos = self.size + offset
        else:
            raise ValueError(f"invalid value for whence: {whence}")
        if self._pos < 0:
            self._pos = 0
        else:
            self._pos = min(self._pos, self.size)
        return self._pos

    def get_current_chunk(self):
        chunk_n = self._pos // self.CHUNK_SIZE
        start = chunk_n * self.CHUNK_SIZE
        if chunk_n not in self._chunks:
            self._logger.debug("fetch chunk %d", chunk_n)
            end = min(start + self.CHUNK_SIZE, self.size) - 1
            r = requests.get(self.url, headers={"Range": f"bytes={start}-{end}"})
            r.raise_for_status()
            self._chunks[chunk_n] = r.content
        return start, self._chunks[chunk_n]

    def read(self, size=-1):
        if size < 0:
            size = self.size - self._pos
        end_pos = self._pos + size
        self._logger.debug("reading %d bytes from %d", size, self._pos)
        buf = []
        while self._pos < end_pos:
            base, chunk = self.get_current_chunk()
            chunk = chunk[self._pos - base : end_pos - base]
            self._pos += len(chunk)
            buf.append(chunk)
        return b"".join(buf)


class Node:
    """
    Recursive structure to represent test results in a tree.
    """

    def __init__(self, name, data=None):
        self.name = name
        self.children = []
        self.parent = None
        self.data = data

    def add(self, node):
        """Add child node to the current node"""
        assert node.parent is None
        node.parent = self
        self.children.append(node)
        self.children.sort(key=lambda node: node.name)
        return node

    @property
    def id(self):
        """Node id as full path to the element"""
        if self.parent is None:
            return self.name
        else:
            return f"{self.parent.id}.{self.name}"

    def __getitem__(self, path):
        """
        Find a node in the tree by path.

        Raise KeyError if not found.
        """
        try:
            head, tail = path.split(".", 1)
        except ValueError:  # no "."
            head, tail = path, None

        try:
            for node in self.children:
                if node.name == head:
                    if tail:
                        return node[tail]
                    else:
                        return node
        except KeyError:
            pass
        raise KeyError(f"node {path} not found")

    def get(self, path):
        """
        Look for a node in the tree by path.

        Return None if not found.
        """
        try:
            return self[path]
        except KeyError:
            return None

    def make(self, path):
        """
        Get a node by path, or create a default one if missing (including all
        required levels).
        """
        if path is None:
            return self

        try:
            head, tail = path.split(".", 1)
        except ValueError:  # no "."
            head, tail = path, None

        return (self.get(head) or self.add(Node(head))).make(tail)

    def __str__(self):
        out = [self.name]
        for child in self.children:
            out.extend("  " + line for line in str(child).splitlines())
        return "\n".join(out)

    def to_dict(self):
        """
        Convert the tree to a series of nested dictionaries.
        """
        return {
            "name": self.name,
            "data": self.data,
            "children": [node.to_dict() for node in self.children],
        }

    @classmethod
    def from_dict(cls, data):
        """
        Recreate a tree from nested dictionaries produced by Node.to_dict().
        """
        node = cls(data["name"], data.get("data"))
        for sub in data.get("children", []):
            node.add(cls.from_dict(sub))
        return node


class TestResults:
    """
    Class representing test results.
    """

    def __init__(self):
        self.stats = {}
        self.build_tool = None
        self.site = {}
        self.start_time = 0
        self.end_time = 0
        self.tests = Node("(root)")

    @classmethod
    def from_ctest(cls, xml):
        """
        Construct and fill a TestResults instance from CTest XML
        (parsed with ElementTree).
        """

        results = cls()
        results.build_tool = "cmake"
        results.stats = {
            k: len(list(g))
            for k, g in groupby(
                sorted(
                    n.attrib["Status"] for n in xml.findall("./Testing/Test[@Status]")
                )
            )
        }

        results.site = dict(
            xml.getroot().attrib if hasattr(xml, "getroot") else xml.attrib
        )
        results.start_time = int(xml.find("./Testing/StartTestTime").text)
        results.end_time = int(xml.find("./Testing/EndTestTime").text)

        for test in xml.findall("./Testing/Test"):
            measurements = {
                meas.attrib["name"]: (
                    float
                    if meas.attrib["type"] == "numeric/double"
                    else Markup.unescape
                )(meas.find("Value").text)
                for meas in test.findall("Results/NamedMeasurement")
            }

            output = test.find("Results/Measurement/Value")
            if output.text:  # text might be None
                out_data = output.text.encode()

                if output.attrib.get("encoding") == "base64":
                    out_data = base64.b64decode(out_data)

                if output.attrib.get("compression") == "gzip":
                    out_data = zlib.decompress(out_data)

                measurements["output"] = out_data.decode()

            else:
                measurements["output"] = ""

            results.tests.make(test.find("Name").text).data = {
                "Status": test.attrib["Status"],
                "CTest Full Command Line": test.find("FullCommandLine").text,
                "Labels": [label.text for label in test.findall("./Labels/Label")],
                "results": measurements,
            }

            # extract the Jenkins BUILD_URL (if any) from the tests' environment
            if not "BUILD_URL" in results.site and "Environment" in measurements:
                for l in measurements["Environment"].splitlines():
                    if l.startswith("BUILD_URL="):
                        results.site["BUILD_URL"] = l[10:].strip()
                        break

        return results

    @classmethod
    def from_cmt(cls, root: Union[pathlib.Path, zipfile.Path]):
        results = cls()
        results.build_tool = "cmt"

        OUTCOMES = {
            "ERROR": "error",
            "FAIL": "failed",
            "PASS": "passed",
            "UNTESTED": "untested",
        }
        FIELDS_MAP = {
            "ExecTest.exit_code": "Exit Value",
            "ExecTest.stderr": "stderr",
            "ExecTest.stdout": "output",
            "qmtest.cause": "Causes",
        }

        # we only care about the content of the "results" directory
        root /= "results"

        summary = json.load((root / "summary.json").open())
        results.stats = {
            OUTCOMES.get(k, "error"): len(list(g))
            for k, g in groupby(sorted(t["outcome"] for t in summary))
        }
        results.site = json.load((root / "annotations.json").open())

        for test in summary:
            measurements = {
                FIELDS_MAP.get(field, field): (root / test["id"] / field).read_text()
                for field in test["fields"]
            }
            results.tests.make(test["id"]).data = {
                "Status": OUTCOMES.get(test["outcome"], "error"),
                "results": measurements,
            }
        return results

    def to_dict(self):
        return {
            "stats": self.stats,
            "site": self.site,
            "build_tool": self.build_tool,
            "tests": self.tests.to_dict(),
        }

    @classmethod
    def from_dict(cls, data):
        results = cls()
        results.stats = data.get("stats", {})
        results.site = data.get("site", {})
        results.build_tool = data.get("build_tool", "cmake")
        if "tests" in data:
            results.tests = Node.from_dict(data["tests"])
        return results


def summarize_test_report(test_xml):
    """
    Convert CTest Test.xml report to JSON
    """
    from xml.etree import ElementTree as ET

    xml = ET.fromstring(test_xml)
    data = TestResults.from_ctest(xml)
    return data.to_dict()


def summarize_test_report_cmt(root: Union[pathlib.Path, zipfile.Path]):
    """
    Convert CMT/QMTest report to JSON
    """
    data = TestResults.from_cmt(root)
    return data.to_dict()


def parse_id(slot_id):
    """
    Split a slot id in the form "[flavour/]slot/build_id" in a tuple (flavour,
    slot, build_id) where flavour is "nightly" if omitted, and build_id is
    converted to integer.
    """
    parts = slot_id.split("/")
    try:
        flavour, slot, build_id = parts
    except ValueError:  # probably flavour was omitted
        flavour = "nightly"
        slot, build_id = parts
    build_id = int(build_id)
    return flavour, slot, build_id


def legacy_artifact(type, flavour, slot, build_id, project, version, platform=None):
    """
    Helper to compute the legacy artifact path.
    """
    base = f"{flavour}/{slot}/{build_id}"
    if type == "checkout":
        platform = "src"
    elif type == "shared":
        platform = "shared"
    elif type == "tests":
        return f"{base}/tests/{platform}/cdash/{project}.zip"
    elif type == "tests-html":
        return f"{base}/tests/{platform}/{project}.zip"
    # else type == "build":
    #     pass
    return (
        f"{base}/packs/{platform}/{project}.{version}.{slot}.{build_id}.{platform}.zip"
    )
