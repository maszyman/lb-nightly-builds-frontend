import datetime
import logging
import re
import time
from collections import defaultdict

from flask import request
from lb.nightly.db import connect
from werkzeug.exceptions import NotFound

from .application import app
from .utils import legacy_artifact

# lb.nightly.db.database.Database instance and creation time
# so that we can refresh it avery now and then to dump the cache
_db_instance = (None, 0)
# max age of the database instance before it has to be dropped (seconds)
_DB_REFRESH_TIME = 10 if app.config["DEBUG"] or app.config["TESTING"] else 60

if not app.config["COUCHDB_URL"]:
    logging.warning("COUCHDB_URL not set or empty: working in legacy-only mode")


class _dummy_db:
    """
    Dummy database class to use when operate in "legacy-only" mode.
    """

    _instance = None

    def __init__(self):
        class internal:
            @staticmethod
            def all_docs(*args, **kwargs):
                return {"rows": []}

        self._db = internal

    @classmethod
    def instance(cls):
        if cls._instance is None:
            cls._instance = cls()
        return cls._instance

    def __getitem__(*args, **kwargs):
        raise KeyError("not implemented")

    def __contains__(*args, **kwargs):
        return False

    def latestSlotsBuilt(*args, **kwargs):
        return {}

    def slotsForDay(*args, **kwargs):
        return []

    def slotBuilds(*args, **kwargs):
        return []

    def slotDocs(*args, **kwargs):
        return []

    def slotsSinceDay(*args, **kwargs):
        return []


def db():
    if not app.config["COUCHDB_URL"]:
        return _dummy_db.instance()
    global _db_instance
    instance, creation = _db_instance
    now = time.time()
    if (now - creation) > _DB_REFRESH_TIME:  # instance last update more than X s ago
        instance, creation = connect(app.config["COUCHDB_URL"]), now
        _db_instance = (instance, creation)
    return instance


_legacy_dbs = {}


def legacy_db(flavour):
    from cloudant.client import CouchDB
    from lb.nightly.db.utils import parse_connection_args

    global _legacy_dbs
    instance, creation = _legacy_dbs.get(flavour, (None, 0))
    now = time.time()
    if (now - creation) > _DB_REFRESH_TIME:  # instance last update more than X s ago
        # LEGACY_COUCHDB_URL does not contain the database name, but parse_connection_args
        # requires it so we use a dummy value
        args, _db = parse_connection_args(app.config["LEGACY_COUCHDB_URL"] + "/-")
        server = CouchDB(
            url=args["url"],
            user=args["user"],
            auth_token=args["auth_token"],
            connect=True,
            auto_renew=True,
            admin_party=not args["user"],
        )
        instance, creation = server[f"nightlies-{flavour}"], now
        _legacy_dbs[flavour] = (instance, creation)
    return instance


def get_slot_info(flavour, slot, build_id):
    try:
        try:
            return db()[flavour, slot, build_id]
        except KeyError:
            if not app.config.get("LEGACY_COUCHDB_URL"):
                raise
            data = legacy_db(flavour)[f"{slot}.{build_id}"]
            data["legacy"] = True
            data["flavour"] = flavour
            versions = {p["name"]: p["version"] for p in data["config"]["projects"]}
            for name, proj in data.get("checkout", {}).get("projects", {}).items():
                if proj.get("completed"):
                    proj["artifact"] = legacy_artifact(
                        "checkout", flavour, slot, build_id, name, versions[name]
                    )
            config = data.get("config", {})
            config_projects = {
                proj.get("name"): proj for proj in config.get("projects", [])
            }
            for platform in config.get("platforms", []):
                for name, proj in data.get("builds", {}).get(platform, {}).items():
                    if (
                        proj.get("completed")
                        and name != "info"
                        and not config_projects.get(name, {}).get(
                            "platform_independent"
                        )
                    ):
                        proj["artifact"] = legacy_artifact(
                            "build",
                            flavour,
                            slot,
                            build_id,
                            name,
                            versions[name],
                            platform,
                        )

            return data
    except KeyError as err:
        raise NotFound(f"cannot find {err}")


def slot_summary(raw, with_test_names=False):
    """
    Return a reduced summary for the slot, ready to be formatted as a table.
    """

    def summarize_checkout(proj):
        data = raw.get("checkout", {}).get("projects", {}).get(proj["name"])
        output = {}
        if data:
            if "completed" in data:
                output = {
                    k: data.get(k, [])
                    or data.get(
                        k[:-1], []
                    )  # checkout reports used to have "warning" and not "warnings"
                    for k in ("warnings", "errors")
                }
                output["retcode"] = data.get("retcode", 0)
                output["artifact"] = data.get("artifact")
            elif "started" in data:
                if "build_url" in data:
                    build_url = data["build_url"]
                elif "worker_task_id" in data:
                    build_url = app.config["WORKER_LOGS_TEMPLATE_URL"].safe_substitute(
                        worker_task_id=data.get("worker_task_id")
                    )
                elif "build_url" in raw.get("checkout", {}):
                    build_url = raw["checkout"]["build_url"]
                else:
                    build_url = None
                output = {"running": build_url}

        if not proj.get("disabled") and proj.get("checkout_opts", {}).get("merges", []):
            merge_requests = []
            for merge in proj["checkout_opts"]["merges"]:
                try:
                    iid, _commit = merge
                    merge_requests.append(iid)
                except Exception as err:
                    logging.warning(
                        f"{raw['flavour']}/{raw['slot']}/{raw['build_id']}:{proj['name']}: bad MR entry {merge!r}"
                    )
            output["merge_requests"] = merge_requests

        return output

    def summarize_build(proj, plat):
        data = raw.get("builds", {}).get(plat, {}).get(proj["name"])
        output = None
        if data:
            if "completed" in data:
                output = {k: data.get(k, 0) for k in ("warnings", "errors", "retcode")}
                output["artifact"] = data.get("artifact")
            elif "started" in data:
                if "build_url" in data:
                    build_url = data["build_url"]
                elif "worker_task_id" in data:
                    build_url = app.config["WORKER_LOGS_TEMPLATE_URL"].safe_substitute(
                        worker_task_id=data.get("worker_task_id")
                    )
                else:
                    build_url = None
                output = {"running": build_url}
        return output

    def summarize_test(proj, plat):
        data = raw.get("tests", {}).get(plat, {}).get(proj["name"])
        if data:
            if data.get("completed"):
                data = {
                    k: data.get("results", {}).get(k, [])
                    for k in ("PASS", "FAIL", "ERROR")
                }
                if "ERROR" in data:
                    # we count ERROR as FAIL in the summary
                    data["FAIL"] = data.get("FAIL", []) + data.pop("ERROR")
                if not with_test_names:
                    data = {k: len(data[k]) for k in data}
            elif "started" in data:
                if "build_url" in data:
                    build_url = data["build_url"]
                elif "worker_task_id" in data:
                    build_url = app.config["WORKER_LOGS_TEMPLATE_URL"].safe_substitute(
                        worker_task_id=data.get("worker_task_id")
                    )
                else:
                    build_url = None
                data = {"running": build_url}
        return data

    config = raw["config"]
    platforms = config["platforms"]

    if not raw.get("legacy"):
        flavour = raw["_id"].split(":", 1)[0]
        slot_id = raw["_id"].replace(":", "/")
    else:
        flavour = raw["flavour"]
        slot_id = f"{flavour}/{config['slot']}/{config['build_id']}"

    def get_scheduler_url():
        """
        if the slot is still running, return the url to dependency graph,
        otherwise link to task history (because graph unavailable anymore)
        """

        for proj in config["projects"]:
            if "running" in summarize_checkout(proj):
                return app.config["SCHEDULER_GRAPH_TEMPLATE_URL"].safe_substitute(
                    scheduler_task_id=raw.get("scheduler_task_id", "")
                )
            for plat in platforms:
                if "running" in (summarize_build(proj, plat) or {}) or "running" in (
                    summarize_test(proj, plat) or {}
                ):
                    return app.config["SCHEDULER_GRAPH_TEMPLATE_URL"].safe_substitute(
                        scheduler_task_id=raw.get("scheduler_task_id", "")
                    )
        return app.config["SCHEDULER_TASK_TEMPLATE_URL"].safe_substitute(
            slot_id=slot_id
        )

    summary = {
        "id": slot_id,
        "name": config["slot"],
        "build_id": config["build_id"],
        "flavour": flavour,
        "date": raw.get("date"),
        "description": config["description"],
        "build_tool": config.get("build_tool"),
        "deployment": config.get("deployment"),
        "platforms": platforms,
        "projects": [
            {
                "name": proj["name"],
                "version": proj["version"],
                "enabled": not proj["disabled"],
                # disabled implies no_test and no_build
                "no_test": proj["disabled"] or proj.get("no_test", False),
                "no_build": proj["disabled"] or proj.get("platform_independent", False),
                "checkout": summarize_checkout(proj),
                "results": {
                    plat: {
                        "build": summarize_build(proj, plat),
                        "tests": summarize_test(proj, plat),
                    }
                    for plat in platforms
                },
            }
            for proj in config["projects"]
        ],
        "legacy": raw.get("legacy", False),
        "aborted": raw.get("aborted"),
        "scheduler_url": get_scheduler_url(),
    }

    # add list of packages to container projects
    packages = defaultdict(list)
    for package in config["packages"]:
        packages[package["container"]].append(
            {"name": package["name"], "version": package["version"]}
        )

    for project in summary["projects"]:
        if project["name"] in packages:
            project["packages"] = packages[project["name"]]

    if "ci_test" in config.get("metadata", {}):
        summary["ci_test"] = config["metadata"]["ci_test"]

    if config.get("metadata", {}).get("to_build"):
        summary["build_pending"] = True

    return summary


def get_slot_summary(flavour, slot, build_id, with_test_names=False):
    """
    Return a reduced summary for the slot, ready to be formatted as a table.
    """
    if "with_tests" in request.args:
        with_test_names = request.args["with_tests"].lower() in ("1", "t", "true")
    return slot_summary(
        get_slot_info(flavour, slot, build_id), with_test_names=with_test_names
    )


def _legacy_latestSlotsBuilt(flavour, since):
    if not app.config.get("LEGACY_COUCHDB_URL"):
        return {}
    if since is not None:
        # Note: timestamps in JS are in milliseconds
        since = int((since - datetime.date.fromtimestamp(0)).total_seconds() * 1000)
    else:
        since = 0

    return {
        entry["key"]: {
            "build_id": entry["value"][0]["max"],
            "id": "{}.{}".format(entry["key"], entry["value"][0]["max"]),
        }
        for entry in legacy_db(flavour).get_view_result(
            "_design/summaries_new", "latest_builds", group=True, raw_result=True
        )["rows"]
        if entry["value"][1]["max"] >= since
    }


def get_latest_slots_summaries(flavour: str, since: datetime.date = None):
    legacy_slots = _legacy_latestSlotsBuilt(flavour, since)
    slots = db().latestSlotsBuilt(flavour, since)

    # drop duplicates between databases (keep the highest id)
    for key in set(slots).intersection(legacy_slots):
        if slots[key]["build_id"] >= legacy_slots[key]["build_id"]:
            del legacy_slots[key]
        else:
            del slots[key]

    docs = [
        row["doc"]
        # FIXME: we should add bulk doc retrieval to lb.nightly.db
        for row in db()._db.all_docs(
            keys=[value["id"] for value in slots.values()], include_docs=True
        )["rows"]
    ]

    if legacy_slots:
        legacy_docs = [
            row["doc"]
            for row in legacy_db(flavour).all_docs(
                keys=[value["id"] for value in legacy_slots.values()], include_docs=True
            )["rows"]
        ]
        for doc in legacy_docs:
            doc.update((("legacy", True), ("flavour", flavour)))
        docs.extend(legacy_docs)

    docs.sort(key=lambda doc: (doc["slot"], doc["build_id"]))
    for doc in docs:
        yield slot_summary(doc)


def _legacy_slotsForDay(day, flavour):
    if not app.config.get("LEGACY_COUCHDB_URL"):
        return []
    return [
        row["value"]
        for row in legacy_db(flavour).get_view_result(
            "_design/summaries_new", "by_day", key=str(day)
        )
    ]


def get_day_slots_summaries(flavour: str, day: datetime.date):
    slots = sorted(
        set(
            (info["slot"], info["build_id"])
            for info in db().slotsForDay(day, flavour)
            + _legacy_slotsForDay(day, flavour)
        )
    )
    for slot, build_id in slots:
        yield get_slot_summary(flavour, slot, build_id)


def _legacy_slotsBuilds(name, flavour, min_id):
    if not app.config.get("LEGACY_COUCHDB_URL"):
        return []
    return [
        (entry["build_id"], entry["date"])
        for entry in legacy_db(flavour).get_query_result(
            selector={"slot": name, "build_id": {"$gte": min_id}},
            fields=["build_id", "date"],
        )
        if "date" in entry  # some test slots may not have a date (not started)
    ]


def latest_slot_build_ids(flavour, slot, min_id):
    return sorted(
        set(
            db().slotBuilds(slot, flavour=flavour, min_id=min_id)
            + _legacy_slotsBuilds(slot, flavour=flavour, min_id=min_id)
        )
    )


def get_periodic_tests(n_of_entries):
    # This query requires the index
    # {
    #   "_id": "_design/index-by-start",
    #   "language": "query",
    #   "views": {
    #     "time_start-json-index": {
    #       "map": {
    #         "fields": {
    #           "time_start": "desc"
    #         },
    #         "partial_filter_selector": {}
    #       },
    #       "reduce": "_count",
    #       "options": {
    #         "def": {
    #           "fields": [
    #             {
    #               "time_start": "desc"
    #             }
    #           ]
    #         }
    #       }
    #     }
    #   }
    # }
    docs = legacy_db("periodic").get_query_result(
        selector={"time_start": {"$gt": None}}, sort=[{"time_start": "desc"}]
    )[:n_of_entries]
    for doc in docs:
        doc["cls"] = "success"
        doc["status_display"] = "Success"
        if not doc["status"] or doc["status"] == "running":
            doc["cls"] = "info"
            doc["status_display"] = "Running"
        elif int(doc["status"]) > 0:
            doc["cls"] = "danger"
            doc["status_display"] = "Failed with exit code: %s" % doc["status"]
        elif int(doc["status"]) == 0 and doc.get("handlers_info", None):
            for handler in doc["handlers_info"]:
                if handler["successful"] is False:
                    doc["cls"] = "warning"
                    doc["status_display"] = "Success but handlers failed"
                    break
        for key in ("time_start", "time_end", "app_version_datetime"):
            if key in doc:
                doc[key] = doc[key].replace("_", " ")

    return docs


def get_slot_page(flavour, name, page, legacy=False):
    """
    Return a "page" summaries of slot builds (from newer to older).

    Note: only one DB (legacy or new) can be used in a call
    """
    PAGE_SIZE = 10
    if not legacy:
        for doc in db().slotDocs(name, flavour)[
            PAGE_SIZE * page : PAGE_SIZE * (page + 1)
        ]:
            yield slot_summary(doc)
    else:
        if not app.config.get("LEGACY_COUCHDB_URL"):
            return []
        docs = legacy_db(flavour).get_query_result(
            selector={"slot": name}, sort=[{"build_id": "desc"}]
        )[PAGE_SIZE * page : PAGE_SIZE * (page + 1)]
        for doc in docs:
            doc.update((("legacy", True), ("flavour", flavour)))
            yield slot_summary(doc)


def _legacy_slots_list(since, flavour):
    if not app.config.get("LEGACY_COUCHDB_URL"):
        return []
    return [
        row["value"]
        for row in legacy_db(flavour).get_view_result(
            "_design/summaries_new", "by_day", startkey=str(since)
        )
    ]


def get_slots_since_day(flavour, since):
    """
    Get basic infos for the slots built since a day.
    """
    return [s for _, s in db().slotsSinceDay(since, flavour)] + _legacy_slots_list(
        since, flavour
    )
