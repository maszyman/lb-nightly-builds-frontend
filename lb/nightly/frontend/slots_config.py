import logging
import os
from multiprocessing import Pipe, Process
from numbers import Integral
from tempfile import TemporaryDirectory
from typing import Iterable, Mapping, Optional, Tuple, Union

import requests
from git import Repo
from gitlab import Gitlab, GitlabGetError
from lb.nightly.configuration import DataProject, Package, Project, Slot, loadConfig

from .application import app, cache

ProjectId = Union[str, Integral]


_logger = logging.getLogger(__name__)


def _parse_config_id(config: Optional[str]) -> Tuple[str, str]:
    """
    Helper to convert a string like ``<git repo>[#<commit id>]`` to a pair
    ``(<git repo>, <commit id>)`` taking into account the default values.
    """
    url, commit = app.config["SLOTS_CONFIG_REPOSITORY"], config

    if config is not None:
        if "#" in config:  # url + commit
            url, commit = config.rsplit("#", 1)
        elif "://" in config:  # only url, default commit
            url, commit = config, None

    return url, commit


@cache.memoize(timeout=3600, unless=lambda _, config_id: config_id is None)
def get_all_slots(config_id: Optional[str] = None) -> Mapping[str, Slot]:
    # Since the configuration files are importable Python modules, and we need
    # to use different versions of the configuration repository, we clone the
    # repository in a temporary directory, then we import the modules in a
    # different process and pass the requested slots to the parent process.
    try:
        with TemporaryDirectory() as tmpdir:
            url, commit = _parse_config_id(config_id)
            repo = Repo.clone_from(
                url,
                os.path.join(tmpdir, "configs"),
                no_checkout=True,
            )
            if commit:
                # we assume "commit" is a commit id accessible from a plain
                # "git clone" (so no commits from merge requests coming from
                # forks, or non-default branch name)
                repo.head.reference = commit
            # check out the files
            repo.head.reset(index=True, working_tree=True)

            def f(conn, tmpdir):
                try:
                    os.chdir(tmpdir)
                    slots = loadConfig()
                    conn.send(slots)
                except Exception as err:
                    print(err)
                    conn.send({})

            parent_conn, child_conn = Pipe()
            p = Process(target=f, args=(child_conn, tmpdir))
            p.start()
            slots = parent_conn.recv()
            p.join()

            return slots

    except Exception as err:
        _logger.error("make_slots: %s: %s", type(err).__name__, err)
        return {}


def get_slots(
    names: Union[None, str, Iterable[str]] = None, config_id: Optional[str] = None
) -> Mapping[str, Optional[Slot]]:
    """Return a map of slot instance for the given name.

    :param names: names of the slots to extract from the configuration, if t's a
        string then get only that slot, if it is None get all configured slots
    :param config_id: identifier of the version of the configuration to use,
        either ``None`` to get the latest default version, or a commit id in the
        default repository or a string like ``<git repo>[#<commit id>]``.
    """
    slots = get_all_slots(config_id)
    if names is None:
        return slots
    if isinstance(names, str):
        names = [names]
    return {name: slots.get(name) for name in names}


@cache.memoize()
def _gitlabServer() -> Gitlab:
    return Gitlab(app.config["GITLAB_URL"], private_token=app.config["GITLAB_TOKEN"])


@cache.memoize()
def _getGitlabProject(project_id: ProjectId):
    return _gitlabServer().projects.get(project_id)


@cache.memoize()
def getMRCommit(project_id: ProjectId, mr_iid: Integral):
    mr = _getGitlabProject(project_id).mergerequests.get(mr_iid)
    for c in mr.commits():
        return c.id
    return None


@cache.memoize()
def _getAllProjectMRs(project_id: ProjectId):
    return _getGitlabProject(project_id).mergerequests.list(state="opened", all=True)


def getMergeRequests(project_id: ProjectId, labels=None, wip=None, target_branch=None):
    wip_condition = (
        (lambda _: True) if wip is None else (lambda mr: mr.work_in_progress == wip)
    )
    if labels:
        labels = set(labels)
        label_condition = lambda mr: labels.intersection(mr.labels)
    else:
        label_condition = lambda _: True
    if not target_branch:
        target_branch = _getGitlabProject(project_id).default_branch
    condition = lambda mr: (
        mr.target_branch == target_branch and wip_condition(mr) and label_condition(mr)
    )
    try:
        mrs = [mr for mr in _getAllProjectMRs(project_id) if condition(mr)]
        mrs.sort(key=lambda mr: mr.iid)
        return mrs

    except GitlabGetError:
        _logger.warning("problem getting MRs for %s", project_id)
        return []


def getProjectId(project: Union[Project, Package]) -> str:
    """
    Given a Project instance return the corresponding Gitlab project id
    ('group/name').
    """
    project_id = project.name

    if "url" in project.checkout_opts:
        if project.checkout_opts["url"].startswith(app.config["GITLAB_URL"]):
            project_id = project.checkout_opts["url"][len(app.config["GITLAB_URL"]) :]
            if project_id.endswith(".git"):
                project_id = project_id[:-4]

    if isinstance(project, Package):
        project_id = "lhcb-datapkg/" + project_id
    elif "/" not in project_id:
        # FIXME: we should get the gitlab group from soft conf db
        project_id = "{}/{}".format(
            {
                "Gaudi": "gaudi",
                "Gaussino": "Gaussino",
            }.get(project_id, "lhcb"),
            project_id,
        )

    return project_id


@cache.memoize()
def getBranchCommitId(project_id: ProjectId, branch: str):
    project = _getGitlabProject(project_id)
    return project.branches.get(branch).commit["id"]


def resolveProjectMRs(project: Union[Project, Package]):
    """
    Given a project, convert the list of requested MRs to a list
    of (mr_iid, commit_id) pairs.

    The supported merge requests are:
    - 'all' (same as 'label=all-slots' + 'label=<slot-name>',
      if the project is in a slot)
    - 'label=<abc>'
    - '<id>'

    If the project version is 'HEAD', it's equivalent to version == 'master'
    and adding 'all' to the merges.

    The project is not modified.
    """
    project_id = getProjectId(project)
    target_branch = project.version

    merges = project.checkout_opts.get("merges", [])
    if not isinstance(merges, list):
        merges = [merges]
    else:
        merges = list(merges)

    if target_branch.lower() == "head":
        target_branch = "master"
        merges.insert(0, "all")

    # if the list ove merge requests already contain *resolved* merge requests
    # we do not need to re-resolve them (and we do not want to include them twice)
    already_resolved = {
        merge[0]
        for merge in merges
        if isinstance(merge, (list, tuple)) and len(merge) == 2
    }

    new_merges = []
    for m in merges:
        if m == "all":
            tmp = getMergeRequests(
                project_id, labels=("all-slots",), target_branch=target_branch
            )
            if project.slot:
                tmp.extend(
                    getMergeRequests(
                        project_id,
                        labels=(project.slot.name,),
                        target_branch=target_branch,
                    )
                )
            tmp = [
                (mr.iid, getMRCommit(project_id, mr.iid))
                for mr in tmp
                if mr.iid not in already_resolved
            ]
            tmp.sort()
            new_merges.extend(tmp)
        elif isinstance(m, Integral):
            new_merges.append((m, getMRCommit(project_id, m)))
        elif isinstance(m, str) and m.startswith("label="):
            new_merges.extend(
                (mr.iid, getMRCommit(project_id, mr.iid))
                for mr in getMergeRequests(
                    project_id, labels=(m[6:],), target_branch=target_branch
                )
                if mr.iid not in already_resolved
            )
        else:
            # when we get resolved MRs from JSON they are lists, but
            # we use tuples here, so we convert them to make the
            # deduplication step below work
            if isinstance(m, list):
                m = tuple(m)
            new_merges.append(m)

    # deduplicate preserving the order
    merges = []
    for m in new_merges:
        if m not in merges:
            merges.append(m)

    return merges


def resolveMRs(item: Union[Slot, Project, Package, Iterable]):
    if isinstance(item, Slot):
        resolveMRs(item.projects)
    elif isinstance(item, DataProject):
        resolveMRs(item.packages)
    elif isinstance(item, (Project, Package)):
        if isinstance(item, Package) or not item.disabled:
            item.checkout_opts["merges"] = resolveProjectMRs(item)
            if not "commit" in item.checkout_opts:
                try:
                    item.checkout_opts["commit"] = getBranchCommitId(
                        getProjectId(item),
                        ("master" if item.version.lower() == "head" else item.version),
                    )
                except GitlabGetError:
                    # this means the requested version is a tag rather
                    # (not a branch)
                    pass
    else:
        return [resolveMRs(p) for p in item]
    return item


def getRequestedStacks():
    """
    Return the list of stack definitions from merge requests in lhcb-core/lhcbstacks.
    """
    from itertools import chain

    project_id = "lhcb-core/lhcbstacks"
    project = _getGitlabProject(project_id)

    # build a dictionaries of stack names to make sure that we have only
    # one definition per name (if )
    stacks = {
        stack["name"]: (stack, mr.iid)
        for stack, mr in chain.from_iterable(
            [
                (stack, mr)
                for stack in requests.get(
                    job.web_url + "/artifacts/raw/to_release.json"
                ).json()
            ]
            # for each ready merge request
            for mr in getMergeRequests(project_id, wip=False)
            # take the latest successful pipeline (they come from latest to first)
            for pipeline in [
                pl for pl in mr.pipelines.list() if pl.status == "success"
            ][:1]
            # find the "generate_stacks" job
            for job in project.pipelines.get(pipeline.id).jobs.list(all=True)
            if job.name == "generate_stacks"
        )
    }
    for stack, mr_id in stacks.values():
        stack["merge_request"] = mr_id
    return [stack for stack, _ in stacks.values()]


def stackToSlot(stack):
    """
    Create a Slot instance from a stack definition.
    """
    desc = "release build for stack {name}".format(**stack)
    if "merge_request" in stack:
        desc = '{} (from <a href="https://gitlab.cern.ch/lhcb-core/lhcbstacks/-/merge_requests/{merge_request}" target="_blank">lhcb-core/lhcbstacks!{merge_request}</a>)'.format(
            desc, **stack
        )

    return Slot(
        "lhcb-release",
        desc=desc,
        projects=[
            Project(
                name,
                version,  # FIXME check if the tag exists
                checkout_opts={"export": True},
                disabled=(name in stack["deployed"]),
                with_shared=(name == "Geant4"),
            )
            for name, version in stack["projects"].items()
        ],
        platforms=stack["platforms"],
        build_tool=stack["build_tool"].lower(),
        cache_entries={
            # Special settings required for new style CMake projects
            "GAUDI_USE_INTELAMPLIFIER": True,
            "GAUDI_LEGACY_CMAKE_SUPPORT": True,
        },
        no_patch=True,
        with_version_dir=True,
        metadata={"lhcbstacks": stack},
    )
