from flask import render_template, request
from flask.helpers import url_for
from werkzeug.exceptions import NotFound

from .application import app
from .models import get_slot_summary
from .utils import parse_id


@app.route("/compare")
def compare():
    """
    Compare two slots.

    Only common projects and common platforms are compared.
    """

    a = request.args.get("a")
    b = request.args.get("b")
    if not (a and b):
        raise NotFound("must provide both 'a' and 'b' parameters")

    a = get_slot_summary(*parse_id(a), with_test_names=True)
    b = get_slot_summary(*parse_id(b), with_test_names=True)

    # convert projects lists to dictionaries
    a["projects"] = {project["name"]: project for project in a["projects"]}
    b["projects"] = {project["name"]: project for project in b["projects"]}

    # extract common projects and platforms
    projects = [name for name in a["projects"] if name in b["projects"]]
    platforms = [name for name in a["platforms"] if name in set(b["platforms"])]
    equals = {"build": {}, "tests": {"deltas": {}}}
    diffs = {"build": {}, "tests": {"deltas": {}}}

    data = {
        "projects": projects,
        "platforms": platforms,
        "equals": equals,
        "diffs": diffs,
        "a": a,
        "b": b,
    }

    diffs["version"] = {}
    equals["version"] = {}

    for project in projects:
        diffs["build"][project] = {}
        diffs["tests"][project] = {}
        equals["build"][project] = {}
        equals["tests"][project] = {}

        proj_a, proj_b = a["projects"][project], b["projects"][project]
        if proj_a["version"] != proj_b["version"]:
            diffs["version"][project] = {
                "a": proj_a["version"],
                "b": proj_b["version"],
            }
        else:
            equals["version"][project] = proj_a["version"]

        for platform in platforms:
            if proj_a["no_build"] or proj_b["no_build"]:
                equals["build"][project][platform] = "n/a"
                equals["tests"][project][platform] = "n/a"
                continue

            build_a, build_b = (
                proj_a["results"][platform]["build"],
                proj_b["results"][platform]["build"],
            )
            # artifact name is always different, so we drop it from the comparison
            if build_a:
                build_a.pop("artifact", None)
            if build_b:
                build_b.pop("artifact", None)

            if (
                not build_a
                or not build_b
                or "running" in build_a
                or "running" in build_b
            ):
                equals["build"][project][platform] = "not ready"
            elif build_a != build_b:
                diffs["build"][project][platform] = {
                    kind: {
                        "last": build_b[kind],
                        "delta": build_b[kind] - build_a[kind],
                    }
                    for kind in ["warnings", "errors"]
                }
                if build_b["retcode"] == 0:
                    diffs["build"][project][platform]["retcode"] = {
                        "last": "success",
                        "delta": ("unchanged" if build_a["retcode"] == 0 else "fixed"),
                    }
                else:
                    diffs["build"][project][platform]["retcode"] = {
                        "last": "failure",
                        "delta": ("unchanged" if build_a["retcode"] != 0 else "broken"),
                    }
            else:
                equals["build"][project][platform] = build_a

            if proj_a["no_test"] or proj_b["no_test"]:
                equals["tests"][project][platform] = "n/a"
                continue

            tests_a, tests_b = (
                {
                    k: set(v)
                    for k, v in (proj_a["results"][platform]["tests"] or {}).items()
                },
                {
                    k: set(v)
                    for k, v in (proj_b["results"][platform]["tests"] or {}).items()
                },
            )

            if (
                not tests_a
                or not tests_b
                or "running" in tests_a
                or "running" in tests_b
            ):
                equals["tests"][project][platform] = "not ready"
            elif tests_a != tests_b:
                fail_a = tests_a["FAIL"]
                fail_b = tests_b["FAIL"]
                pass_a = tests_a["PASS"]
                pass_b = tests_b["PASS"]
                all_a = pass_a | fail_a
                all_b = pass_b | fail_b

                diff = {}

                diff["all_a"] = all_a
                diff["all_b"] = all_b

                diff.update(
                    {
                        "unchanged": {
                            "PASS": (pass_b & pass_a),
                            "FAIL": (fail_b & fail_a),
                        },
                        "fixed": (pass_b & fail_a),
                        "broken": (fail_b & pass_a),
                    }
                )
                diff["added"] = {
                    "FAIL": (fail_b - all_a),
                    "PASS": (pass_b - all_a),
                }
                diff["removed"] = {
                    "FAIL": (fail_a - all_b),
                    "PASS": (pass_b - all_b),
                }
                diff["PASS"] = {"last": len(pass_b), "delta": len(pass_b) - len(pass_a)}
                diff["FAIL"] = {"last": len(fail_b), "delta": len(fail_b) - len(fail_a)}
                diffs["tests"][project][platform] = diff
                if project not in diffs["tests"]["deltas"]:
                    diffs["tests"]["deltas"][project] = {}
                diffs["tests"]["deltas"][project][platform] = [
                    {
                        "name": t,
                        "a": {
                            "state": (
                                "pass"
                                if t in pass_a
                                else "fail"
                                if t in fail_a
                                else None
                            ),
                            "url": "{}#{}".format(
                                url_for(
                                    "tests",
                                    flavour=a["flavour"],
                                    slot=a["name"],
                                    build_id=a["build_id"],
                                    project=project,
                                    platform=platform,
                                ),
                                t,
                            )
                            if t in all_a
                            else None,
                        },
                        "b": {
                            "state": (
                                "pass"
                                if t in pass_b
                                else "fail"
                                if t in fail_b
                                else None
                            ),
                            "url": "{}#{}".format(
                                url_for(
                                    "tests",
                                    flavour=b["flavour"],
                                    slot=b["name"],
                                    build_id=b["build_id"],
                                    project=project,
                                    platform=platform,
                                ),
                                t,
                            )
                            if t in all_b
                            else None,
                        },
                    }
                    for t in sorted(
                        (all_a | all_b)
                        - (diff["unchanged"]["PASS"] | diff["unchanged"]["FAIL"])
                    )
                ]
            else:
                equals["tests"][project][platform] = {
                    k: len(tests_a[k]) for k in tests_a
                }

    # hidden feature: add "&json" to the URL to get the data in JSON for debugging
    if "json" in request.args:

        def squash_sets(dct):
            """helper to strip set instances and make dct JSON serializable"""
            for k in dct:
                if isinstance(dct[k], dict):
                    squash_sets(dct[k])
                elif isinstance(dct[k], set):
                    dct[k] = sorted(dct[k])

        squash_sets(data)
        return data

    return render_template(
        "compare.html",
        data=data,
    )
