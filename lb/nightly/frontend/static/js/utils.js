days = ["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"];

function checkoutURL(flavour, slot, build_id, project) {
    return '/logs/checkout/'+ flavour + '/' + slot + '/' + build_id + '/' + project + '/';
}

function buildURL(flavour, slot, build_id, platform, project) {
    return '/logs/build/'+ flavour + '/' + slot + '/' + build_id + '/' + platform + '/' + project + '/';
}

function testsURL(flavour, slot, build_id, platform, project) {
    return '/logs/tests/'+ flavour + '/' + slot + '/' + build_id + '/' + platform + '/' + project + '/';
}

function highlight()
{
    $(".table-responsive table tbody tr").hover
    (
        function()
        {
           $(this).prev().children().css("border-bottom-color", "#777");
           $(this).children().css("border-bottom-color","#777");

        },
        // This second function is our mouse out function
        function()
        {
           $(this).prev().children().css("border-bottom-color","");
           $(this).children().css("border-bottom-color","");
        }
    );
}

function parseMiliseconds(duration){
    var milliseconds = parseInt((duration%1000)/100)
        , seconds = parseInt((duration/1000)%60)
        , minutes = parseInt((duration/(1000*60))%60)
        , hours = parseInt((duration/(1000*60*60))%24);

    hours = (hours < 10) ? "0" + hours : hours;
    minutes = (minutes < 10) ? "0" + minutes : minutes;
    seconds = (seconds < 10) ? "0" + seconds : seconds;

    return hours + " hours and " + minutes +" minutes";
}

function parseDate(value){
    return value.split('T')[1].split('.')[0]
}

function getCookie(name) {
    var cookieValue = null;
    if (document.cookie && document.cookie != '') {
        var cookies = document.cookie.split(';');
        for (var i = 0; i < cookies.length; i++) {
            var cookie = jQuery.trim(cookies[i]);
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) == (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}
function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}


function copyLink(link){
     var textArea = document.createElement("textarea");

      //
      // *** This styling is an extra step which is likely not required. ***
      //
      // Why is it here? To ensure:
      // 1. the element is able to have focus and selection.
      // 2. if element was to flash render it has minimal visual impact.
      // 3. less flakyness with selection and copying which **might** occur if
      //    the textarea element is not visible.
      //
      // The likelihood is the element won't even render, not even a flash,
      // so some of these are just precautions. However in IE the element
      // is visible whilst the popup box asking the user for permission for
      // the web page to copy to the clipboard.
      //

      // Place in top-left corner of screen regardless of scroll position.
      textArea.style.position = 'fixed';
      textArea.style.top = 0;
      textArea.style.left = 0;

      // Ensure it has a small width and height. Setting to 1px / 1em
      // doesn't work as this gives a negative w/h on some browsers.
      textArea.style.width = '2em';
      textArea.style.height = '2em';

      // We don't need padding, reducing the size if it does flash render.
      textArea.style.padding = 0;

      // Clean up any borders.
      textArea.style.border = 'none';
      textArea.style.outline = 'none';
      textArea.style.boxShadow = 'none';

      // Avoid flash of white box if rendered for any reason.
      textArea.style.background = 'transparent';


      textArea.value = link;

      document.body.appendChild(textArea);

      textArea.select();

      try {
        var successful = document.execCommand('copy');
        var msg = successful ? 'successful' : 'unsuccessful';
        toastr.info('Link was copied ' + msg);
      } catch (err) {
        console.log('Oops, unable to copy', err);
      }

      document.body.removeChild(textArea);
    }
