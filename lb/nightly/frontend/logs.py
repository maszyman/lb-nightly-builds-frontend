from collections import defaultdict

import requests
from flask import render_template, request
from werkzeug.exceptions import NotFound

from .api import get_build_log, get_checkout_log, get_tests_summary
from .application import app, cache
from .filters import format_test_body


@app.route("/<flavour>/<slot>/<int:build_id>/<project>/checkout")
@cache.cached(timeout=3600)
def checkout(flavour, slot, build_id, project):
    return render_template(
        "checkout_logs.html",
        flavour=flavour,
        slot=slot,
        build_id=build_id,
        project=project,
        report=get_checkout_log(flavour, slot, build_id, project)[0],
    )


@app.route("/<flavour>/<slot>/<int:build_id>/<project>/<platform>/build")
def build(flavour, slot, build_id, project, platform):
    report = get_build_log(flavour, slot, build_id, project, platform)[0]
    section_body = request.args.get("section_body")
    if section_body:
        for section in report["sections"]:
            if section["id"] == section_body:
                return section["html"]
        else:
            raise NotFound(section_body)

    sections_summaries = defaultdict(lambda: defaultdict(int))
    for issue_type, issues in report["issues"].items():
        for issue in issues:
            for section in issue["where"]:
                sections_summaries[section][issue_type] += len(issue["where"][section])
    for section in report["sections"]:
        if section["id"] in sections_summaries:
            section.update(sections_summaries[section["id"]])
    return render_template(
        "build_log.html",
        flavour=flavour,
        slot=slot,
        build_id=build_id,
        project=project,
        platform=platform,
        report=report,
        full="full" in request.args,
    )


@app.route("/<flavour>/<slot>/<int:build_id>/<project>/<platform>/tests")
def tests(flavour, slot, build_id, project, platform):
    report = get_tests_summary(flavour, slot, build_id, project, platform)[0]
    test_body = request.args.get("test_body")
    if test_body:
        from .utils import TestResults

        results = TestResults.from_dict(report)
        try:
            return "".join(format_test_body(results.tests[test_body]))
        except KeyError as err:
            raise NotFound(str(err))

    return render_template(
        "tests_log.html",
        flavour=flavour,
        slot=slot,
        build_id=build_id,
        project=project,
        platform=platform,
        report=report,
        full="full" in request.args,
    )


@app.route("/utils/test_report")
def render_tests():
    from .utils import summarize_test_report

    if "url" not in request.args:
        return "missing argument <pre>url</pre>"

    return render_template(
        "tests_log.html",
        report=summarize_test_report(requests.get(request.args["url"]).content),
        full=True,
    )
