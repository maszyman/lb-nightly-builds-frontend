Setup
=====


Prerequisites
-------------

To be able to run and develop the *LHCb Nightly Builds Frontend* project, you
need at least Python 3.7 and `Python Poetry <https://python-poetry.org>`_, which
can be installed with a `simple command
<https://python-poetry.org/docs/#installation>`_


Quick Start
-----------

To quickly get the latest version of the code and start the server you can type
the following commands::

    git clone https://gitlab.cern.ch/lhcb-core/lb-nightly-builds-frontend.git
    cd lb-nightly-builds-frontend
    ./utils/runserver

These commands will get the code from the `main repository
<https://gitlab.cern.ch/lhcb-core/lb-nightly-builds-frontend>`_ then set up a
cached virtualenv directory (using the ``poetry`` command) and start the server.
At that point you can connect to http://localhost:5000 to see the frontend.

Changes to the code will be automatically picked up by the server, so you can
start editing the code and just reload the page, while changes to the
environment require that the server is restarted.

.. note:: If the changes saved contain a syntax error, the server must be
   restarted to pick up the fixes.
