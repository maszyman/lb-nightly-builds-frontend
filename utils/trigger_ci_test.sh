#!/bin/bash -xe
# Script used by
#   https://jenkins-lhcb-nightlies.web.cern.ch/job/nightly-builds/job/new-ci-test-poll/

. ./utils/setup.sh

rm -fv params.*.txt
poetry run python ${0/.sh/.py}
