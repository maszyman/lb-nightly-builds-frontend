#!/bin/bash -xe
# Script used by
#   https://jenkins-lhcb-nightlies.web.cern.ch/job/nightly-builds/job/new-release-poll/

. ./utils/setup.sh

rm -fv params.*.txt
poetry run python ${0/.sh/.py}
set +x
for f in $(find -maxdepth 1 -name "params.*.txt") ; do
    echo " === $f ==="
    cat $f
    echo ""
done
