# Prepare a Poetry based local installation to run commands

export POETRY_HOME=$PWD/.poetry
export PATH=${POETRY_HOME}/bin:${PATH}

# if the current python is not >= 3.8, get it from LbEnv
if ! python -c "import sys; exit(0 if sys.version_info >= (3, 8) else 1)" ; then
    export PATH=/cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/stable/linux-64/bin:${PATH}
fi

if ! which poetry >/dev/null 2>&1 ; then
	curl -sSLO https://raw.githubusercontent.com/python-poetry/poetry/master/get-poetry.py
    python get-poetry.py --no-modify-path -y
fi

poetry config --local virtualenvs.in-project true
poetry env use /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/stable/linux-64/bin/python

# install dependencies, if it does not work, try again from scratch
if ! poetry install ; then
    poetry env remove /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/stable/linux-64/bin/python 2>/dev/null || true
    poetry env use /cvmfs/lhcb.cern.ch/lib/var/lib/LbEnv/stable/linux-64/bin/python
    poetry install
fi

export SSL_CERT_FILE=$(poetry run python -m certifi)
$(dirname $0)/patch_certfile ${SSL_CERT_FILE}
