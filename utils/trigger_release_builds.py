#!/usr/bin/env python

import sys
from datetime import date
from pathlib import Path

from cloudant.client import CouchDB

from lb.nightly.frontend.slots_config import getRequestedStacks, stackToSlot


def main():
    try:
        user, auth_token = (
            (Path.home() / "private" / "couchdb-admin").read_bytes().splitlines()
        )
    except Exception:
        exit("failed to get CouchDB credentials")

    server = CouchDB(
        url="https://lhcb-couchdb.cern.ch/",
        user=user,
        auth_token=auth_token,
    )

    all_stacks = getRequestedStacks()
    print("Found {} stacks from merge requests".format(len(all_stacks)))

    server.connect()
    db = server["nightlies-release"]

    if "stacks2" in db:
        old_stacks = db["stacks2"]
    else:
        old_stacks = db.create_document(
            {"_id": "stacks2", "type": "stacks", "value": []}
        )

    stacks = [stack for stack in all_stacks if stack not in old_stacks["value"]]
    print("Found {} stacks to build".format(len(stacks)))

    for slot in [stackToSlot(stack) for stack in stacks]:
        rows = list(
            db.get_view_result(
                "summaries",
                "lastBuildId",
                key="lhcb-release",
                group=True,
            )
        )
        slot.build_id = rows[0]["value"] if rows else 0
        for _attempt in range(5):
            slot.build_id += 1
            data = {
                "_id": "{}.{}".format(slot.name, slot.build_id),
                "type": "slot-info",
                "slot": slot.name,
                "build_id": slot.build_id,
                "data": str(date.today()),
                "config": slot.toDict(),
            }
            # take into account an incompatibility between lb-nightly-configuration and LbNightlyTools
            for p in data["config"]["projects"]:
                if "build_tool" in p and not p["build_tool"]:
                    del p["build_tool"]

            try:
                db.create_document(data, throw_on_exists=True)
                break
            except Exception:
                pass
        else:
            exit("failed to create document in CouchDB")

        with open("params.{}.txt".format(slot.build_id), "w") as params:
            params.write(
                "\n".join(
                    [
                        "flavour=release",
                        "slot=lhcb-release",
                        "slot_build_id={}".format(slot.build_id),
                    ]
                )
            )

    # update the list of stack that we already processed
    for _attempt in range(5):
        try:
            old_stacks["value"] = all_stacks
            old_stacks.save()
            break
        except Exception:
            old_stacks.fetch()  # retry
    else:
        exit("failed to update processed stacks list in CouchdB")


if __name__ == "__main__":
    main()
