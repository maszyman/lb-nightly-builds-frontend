#!/usr/bin/env python

from pathlib import Path

from cloudant.client import CouchDB
from requests import HTTPError

"""
Note: The query used here requires the view from the document:

{
  "_id": "_design/slots_to_build",
  "views": {
    "slots_to_build": {
      "map": "function (doc) {\n  if (doc.type == \"slot-info\" && doc.config.metadata.to_build)\n    emit(doc._id, {\"_id\": doc._id, \"slot\": doc.slot, \"build_id\": doc.build_id});\n}"
    }
  },
  "language": "javascript"
}

which must be present in both nightlies-nightly and nightlies-testing databases.
"""


def main():
    try:
        user, auth_token = (
            (Path.home() / "private" / "couchdb-admin").read_bytes().splitlines()
        )
    except Exception:
        exit("failed to get CouchDB credentials")

    server = CouchDB(
        url="https://lhcb-couchdb.cern.ch/",
        user=user,
        auth_token=auth_token,
    )

    param_files = []
    server.connect()
    for flavour in ["nightly", "testing"]:
        db = server["nightlies-{}".format(flavour)]
        for row in db.get_view_result("_design/slots_to_build", "slots_to_build"):
            entry = row["value"]
            try:
                # not strictly needed, but useful later
                entry["flavour"] = flavour
                # flag the document as taken
                doc = db[entry["_id"]]
                doc["config"]["metadata"]["to_build"] = False
                doc.save()
                # create the parameters file for checkout job
                param_file = "params.{flavour}.{_id}.txt".format(**entry)
                with open(param_file, "w") as params:
                    params.write(
                        "flavour={flavour}\nslot={slot}\nslot_build_id={build_id}\n".format(
                            **entry
                        )
                    )
                param_files.append(param_file)

            except HTTPError:
                print(
                    "WARNING: could schedule start of {flavour}/{slot}/{build_id}".format(
                        **entry
                    )
                )

    print("Triggering {} builds:".format(len(param_files)))
    for f in param_files:
        print("====", f, "====")
        print(open(f).read())


if __name__ == "__main__":
    main()
